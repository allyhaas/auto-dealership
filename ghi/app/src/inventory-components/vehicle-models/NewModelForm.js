import React, {useState} from 'react';

function NewModelForm () {
    const [formData, setFormData] = useState({
        name: '',
        picture_url: '',
        manufacturer_id:'',
    })

    const handleSubmit = async (event) => {
        event.preventDefault()

        const modelUrl = 'http://localhost:8100/api/models/'

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
              'Content-Type': 'application/json',
            },
          };

        const response = await fetch(modelUrl, fetchConfig)

        if (response.ok) {
            setFormData({
                name: '',
                picture_url: '',
                manufacturer_id:''
            });
          }
          else console.log("problem")
    }

    const handleChangeInput = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        })
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div id="employee_create">
                <h3>Add a Model</h3>
                    <form onSubmit={handleSubmit} id="create_employee_form">
                        <div className='mb-2'>
                            <label htmlFor="name" className="form-label">Model Name</label>
                            <input value={formData.name} onChange={handleChangeInput} type="text" name="name" id="name" className="form-control" />
                        </div>
                        <div className='mb-2'>
                            <label htmlFor="picture_url" className="form-label">Picture Url</label>
                            <input value={formData.picture_url} onChange={handleChangeInput} type="url" name="picture_url" id="picture_url" className="form-control" />
                        </div>
                        <div className='mb-2'>
                            <label htmlFor="manufacturer_id" className="form-label">Manufacturer ID</label>
                            <input value={formData.manufacturer_id} onChange={handleChangeInput} type="number" name="manufacturer_id" id="manufacturer_id" className="form-control" />
                        </div>
                        <button className="btn btn-primary">Create a New Model</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default NewModelForm
