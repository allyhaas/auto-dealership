import React, {useState, useEffect} from 'react';


function ManufacturerForm () {
    const [formData, setFormData] = useState({
        name: ''
    })


    const handleSubmit = async (event) => {
        event.preventDefault()
        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(manufacturerUrl, fetchConfig)
        if (response.ok) {
            setFormData({
                name:''
            })
        }
    }


    const handleChangeInput = (e) => {
        const value = e.target.value
        const inputName = e.target.name
        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div id="manufacturer_create">
                    <h1>Add a Manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create_manufacturer_form">
                        <div className='mb-2'>
                            <label htmlFor="name" className="form-label">name</label>
                            <input value={formData.name} onChange={handleChangeInput} type="text" name="name" id="name" className="form-control" />
                        </div>
                        <button className="btn btn-primary" style={{ backgroundColor: "blue", color: "white" }}>Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ManufacturerForm
