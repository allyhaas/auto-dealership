import React, {useState, useEffect} from 'react';


function TechnicianForm () {

    const [technicians, setTechnicians] = useState([])
    const [formData, setFormData] = useState({
        name: '',
        employee_number: ''
    })


    const handleSubmit = async (event) => {
        event.preventDefault()
        const technicianUrl = 'http://localhost:8080/api/technicians/'

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
              'Content-Type': 'application/json',
            },
          };

        const response = await fetch(technicianUrl, fetchConfig)

        if (response.ok) {
            setFormData({
                name: '',
                employee_number: '',
            });
          }
    }


    const handleChangeInput = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div id="technician_create">
                <h3>Add Technician</h3>
                    <form onSubmit={handleSubmit} id="create_technician_form">
                        <div className='mb-2'>
                            <label htmlFor="name" className="form-label">name</label>
                            <input value={formData.name} onChange={handleChangeInput} type="text" name="name" id="name" className="form-control" />
                        </div>
                        <div className='mb-2'>
                            <label htmlFor="employee_number" className="form-label">employee number</label>
                            <input value={formData.employee_number} onChange={handleChangeInput} type="text" name="employee_number" id="employee_number" className="form-control" />
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default TechnicianForm
