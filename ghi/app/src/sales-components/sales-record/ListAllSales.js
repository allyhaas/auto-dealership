import { useEffect, useState } from "react";

function ListAllSales() {
  const [sales, setSales] = useState([]);
  const getData = async () => {
    const response = await fetch("http://localhost:8090/api/sales");
    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
    }
  };
  useEffect(() => {
    getData();
  }, []);
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Customer</th>
          <th>Employee</th>
          <th>VIN </th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>
        {sales?.map((sale) => {
          return (
            <tr key={sale.id}>
              <td>{sale.sales_customer.customer_name}</td>
              <td>{sale.sales_employee.employee_name}</td>
              <td>{sale.automobile.vin}</td>
              <td>{sale.sales_price}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default ListAllSales;
