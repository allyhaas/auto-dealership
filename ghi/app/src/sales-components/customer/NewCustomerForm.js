import React, {useState, useEffect} from 'react';

function NewCustomerForm () {
    const [customers, setCustomers] = useState([])
    const [formData, setFormData] = useState({
        customer_name: '',
        customer_address: '',
        customer_phone: ''
    })

    const handleSubmit = async (event) => {
        event.preventDefault()

        const customerUrl = 'http://localhost:8090/api/customers/'

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
              'Content-Type': 'application/json',
            },
          };

        const response = await fetch(customerUrl, fetchConfig)

        if (response.ok) {
            setFormData({
                customer_name: '',
                customer_address: '',
                customer_phone: ''
            });
          }
    }

    const handleChangeInput = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        })
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div id="customer_create">
                <h3>Add Cusutomer</h3>
                    <form onSubmit={handleSubmit} id="create_customer_form">
                        <div className='mb-2'>
                            <label htmlFor="customer_name" className="form-label">Customer Name</label>
                            <input value={formData.customer_name} onChange={handleChangeInput} type="text" name="customer_name" id="customer_name" className="form-control" />
                        </div>
                        <div className='mb-2'>
                            <label htmlFor="customer_address" className="form-label">Customer Address</label>
                            <input value={formData.customer_address} onChange={handleChangeInput} type="text" name="customer_address" id="customer_address" className="form-control" />
                        </div>
                        <div className='mb-2'>
                            <label htmlFor="customer_phone" className="form-label">Customer Phone Number</label>
                            <input value={formData.customer_phone} onChange={handleChangeInput} type="text" name="customer_phone" id="customer_phone" className="form-control" />
                        </div>
                        <button className="btn btn-primary">Create a New Customer</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default NewCustomerForm
