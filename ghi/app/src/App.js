import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AppointmentList from './service-components/appointments/AppointmentList';
import ServiceHistory from './service-components/service-related/ServiceHistory';
import TechnicianList from './service-components/technicians/TechnicianList';
import TechnicianForm from './service-components/technicians/TechnicianForm';
import AppointmentForm from './service-components/appointments/AppointmentForm';
import ManufacturerList from './inventory-components/manufacturers/ManufacturerList';
import ManufacturerForm from './inventory-components/manufacturers/ManufacturerForm';
import ListModels from './inventory-components/vehicle-models/ListModels';
import ListAllSales from './sales-components/sales-record/ListAllSales';
import NewEmployeeForm from './sales-components/employee/NewEmployeeForm';
import NewCustomerForm from './sales-components/customer/NewCustomerForm';
import EmployeeSalesList from './sales-components/employee/EmployeeSalesList';
import NewSalesForm from './sales-components/sales-record/NewSalesForm';
import NewModelForm from './inventory-components/vehicle-models/NewModelForm';
import ListAllCustomers from './sales-components/customer/ListCustomers';
import AddToInventory from './inventory-components/inventory/AddToInventory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route index element={<MainPage />} />

        <Route path="appointments">
          <Route index element={<AppointmentList />} />
          <Route path="new" element={<AppointmentForm />} />
        </Route>

        <Route path="history">
          <Route index element={<ServiceHistory />} />
        </Route>

        <Route path="technicians">
          <Route index element={<TechnicianList />} />
          <Route path="new" element={<TechnicianForm />} />
        </Route>

        <Route path="manufacturers">
          <Route index element={<ManufacturerList />} />
          <Route path="new" element={<ManufacturerForm />} />
        </Route>
        <Route path="automobiles">
          <Route index element={<AutomobileList />} />
          <Route path="new" element={<AutomobileForm />} />
        </Route>
        
        <Route path="sales">
          <Route index element={<ListAllSales />} />
          <Route path="new" element= {<ListAllSales />} />
        </Route>

        <Route path ="employee-sales">
          <Route index element={<EmployeeSalesList/>} />
          <Route path="new" element= {<EmployeeSalesList />} />
        </Route>

        <Route path ="new-sale">
          <Route index element={<NewSalesForm/>} />
          <Route path="new" element= {<NewSalesForm />} />
        </Route>

        <Route path="models">
          <Route index element={<ListModels />} />
          <Route path="new" element= {<ListModels />} />
        </Route>

        <Route path ="add-employees">
          <Route index element={<NewEmployeeForm/>} />
          <Route path="new" element= {<NewEmployeeForm />} />
        </Route>

        <Route path ="add-customers">
          <Route index element={<NewCustomerForm/>} />
          <Route path="new" element= {<NewCustomerForm />} />
        </Route>

        <Route path ="add-model">
          <Route index element={<NewModelForm/>} />
          <Route path="new" element= {<NewModelForm />} />
        </Route>

        <Route path ="list-customers">
          <Route index element={<ListAllCustomers/>} />
          <Route path="new" element= {<ListAllCustomers />} />
        </Route>

        <Route path ="add-inventory">
          <Route index element={<AddToInventory/>} />
          <Route path="new" element= {<AddToInventory />} />
        </Route>

      </Routes>
    </BrowserRouter>
  );
}

export default App;
