from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=100, null=True, blank=True)
    sales_model = models.CharField(max_length=1000, null=True, blank=True)

    def __str__(self):
        return self.sales_model

class Employee(models.Model):
    employee_name = models.CharField(max_length=100)
    employee_id = models.PositiveSmallIntegerField(unique=True, null=True)

    def __str__(self):
        return self.employee_name

    def get_api_url(self):
        return reverse("show_employee", kwargs={"pk":self.id})

class Customer(models.Model):
    customer_name = models.CharField(max_length=100)
    customer_address = models.CharField(max_length=100)
    customer_phone = models.CharField(max_length=100)

    def __str__(self):
        return self.customer_name

class SalesRecord(models.Model):
    sales_employee = models.ForeignKey(
        Employee,
        related_name="employee",
        on_delete=models.PROTECT
    )
    sales_customer = models.ForeignKey(
        Customer,
        related_name="customer",
        on_delete=models.PROTECT
    )
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.PROTECT
    )
    sales_price = models.PositiveBigIntegerField(null=True, blank=True)

    def __str__(self):
        return self.sales_customer
