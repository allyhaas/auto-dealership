from common.json import ModelEncoder
from .models import SalesRecord, Employee, Customer, AutomobileVO

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO,
    properties = [
        "vin",
        "sales_model",
        "id"
    ]

class EmployeeEncoder(ModelEncoder):
    model = Employee
    properties = [
        "employee_name",
        "id"
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "customer_name",
        "customer_address",
        "customer_phone",
        "id",
    ]

class SalesEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "sales_employee",
        "sales_customer",
        "sales_price",
        "automobile",
        "id"
    ]
    encoders = {
        "automobile" : AutomobileVOEncoder(),
        "sales_customer" : CustomerEncoder(),
        "sales_employee" : EmployeeEncoder(),
    }
