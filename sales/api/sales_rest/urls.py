from django.urls import path
from .views import list_automobiles, list_sales, show_sale, list_customers, show_customer, list_employees, show_employee, delete_automobiles

urlpatterns = [
    path("automobiles/", list_automobiles, name="list_automobiles"),
    path("automobiles/<int:pk>", delete_automobiles, name="list_automobiles"),
    path("sales/", list_sales, name="list_sales"),
    path("sales/<int:pk>/", show_sale, name="show_sale"),
    path("customers/", list_customers, name="list_customers"),
    path("customers/<int:pk>/", show_customer, name="show_customer"),
    path("employees/", list_employees, name="list_employees"),
    path("employees/<int:pk>/", show_employee, name="show_employee"),
]
